float freq = 1;

void setup() {
  size(2460, 256);
  surface.setSize(2463, 280);
  smooth();
  frameRate(24);
}

void draw() {
  handleKeys();
  background(180);

  strokeWeight(1);

  stroke(20, 50, 70);
  int step = 10;
  float lastx = -999;
  float lasty = -999;
  float ynoise = random(10);

  float angle = 0;
  float y = 50;

  for (int x = 20; x < width - 20; x++) {
    angle = 2 * PI * freq * x;
    float rad = radians(angle);
    y = height/2 + (sin(rad) * 40) + noise(20000000 * ynoise + rad * 2) * 30;
    if (lastx > -999) {
      line(x, y, lastx, lasty);
    }
    lastx = x;
    lasty = y;
  }
}

void handleKeys() {
  if (keyPressed) {
    switch (key) {
      case 'q':
        exit();
        break;
      case 'j':
        increaseParam();
        break;
      case 'k':
        decreaseParam();
        break;
    }
  }
}

void increaseParam() {
  freq += 0.1;
  println(freq);
}

void decreaseParam() {
  freq -= 0.1;
  println(freq);
}
